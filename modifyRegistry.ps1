# Copyright 2013 Cloudbase Solutions Srl
#
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

# @author TheBestPessimist

# powershell script which modifies some keys in the registry
# created for cloudbase solutions

Param(
    [switch]$RebootIfModified
)

# working path
$path = "HKLM:\SYSTEM\CurrentControlSet\Control\Class\{4d36e972-e325-11ce-bfc1-08002be10318}\";

# needed in foreach, so that the "get/set -itemProperty" works
$pathPrefix = "Microsoft.PowerShell.Core\Registry::"

# -ea 0 is equivalent to -ErrorAction SilentlyContinue
$searchPath = (Get-ChildItem $path -ea 0).name;

$searchSubject = "VLanFiltering"; 

# tell the user how many registry keys have been modified
$itemsModified = 0
foreach ($i in $searchPath)
{
    # current working item
    $workingPath =  ($pathPrefix + $i);
    $val = (Get-itemProperty -path $workingPath -name $searchSubject -ErrorAction SilentlyContinue).$searchSubject

    if( $val -ne 0 )
    {
        # modify that item
        Set-ItemProperty -path $workingPath -name $searchSubject -value 0;
        $itemsModified += 1;
        # echo ((get-item $workingPath));
    }
}

if ($RebootIfModified -and $itemsModified -gt 0){
    shutdown.exe -r -t 0
}
echo "`n$itemsModified registry keys have been modified.";

